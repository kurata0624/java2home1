import java.util.Scanner;

public class EX0009 {
	
	public static void main(String[]args) {
		
		System.out.println(addUntil(100));
	}
	
	public static int addUntil(int limit) {
		Scanner scanner = new Scanner(System.in);
		
		int sum = 0;
		
		
		do {
			
			System.out.print("> ");
			int input = scanner.nextInt();
			
			sum += input;
			System.out.println(sum);
		}
		while(sum < limit);
		
		return sum;
	}

}
