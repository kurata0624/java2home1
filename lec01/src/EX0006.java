
public class EX0006 {
	
	public static void main(String[]args) {
		
		int[]array1 = {1,3,5,7,9,};
		System.out.println(averageAllElementsValue(array1));
		
		int[]array2 = {11,3,55,7,9,};
		System.out.println(averageAllElementsValue(array2));
		
		int[]array3= {111,3,55,77,999,};
		System.out.println(averageAllElementsValue(array3));
	}
	
	public static double averageAllElementsValue(int[] array) {
		
		return (double)Util.sumAllElementsValue(array)/array.length;
	}
}
