

public class EX0004 {
	public static void main(String[]args) {
		
		System.out.println(sumFrom1To(-1));
		System.out.println(sumFrom1To(0));
		System.out.println(sumFrom1To(1));
		System.out.println(sumFrom1To(100));
	}

	public static int sumFrom1To(int value) {
		if(value < 0) {
			return 0;
		}
		int sum = 0;
		for(int i = 0; i < value; i++) {
			sum += (i +1);
		}
		return sum;
	}
}
