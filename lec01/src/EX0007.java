
public class EX0007 {
	
	public static void main(String[] args) {
		
		System.out.println(compoundInterest(0.0001, 1000000, 2000000));
		System.out.println(compoundInterest(0.05, 1000000, 2000000));
		System.out.println(compoundInterest(0.10, 1000000, 2000000));
		System.out.println(compoundInterest(0.18,1000000, 2000000));
	}
	
	public static int compoundInterest(double rate, double base, double goal) {
		
		double current = base;
		int year = 0;
		
		while(current < goal) {
			
			current *= (1.0 +rate);
			year++;
		}
		
		return year;
	}
}
