
public class EX0008 {
	
	public static void main(String[]args) {
		
		System.out.println(doubleOver(10, 100));
		System.out.println(doubleOver(100, 1234));
		System.out.println(doubleOver(9999, 12345678)); 
	}
	
	public static int doubleOver(int base, int limit) {
		
		int current = base;
		
		while(current < limit) {
			
			current *= 2;
		}
		
		return current;
	}

}
